
#include "image.h"
#include "image_io.h"
#include "vec.h"

void Intro()
{
    Image image(1024, 512);
    
    // exemple : parcours tous les pixels de l'image
    for(unsigned i= 0; i < image.size(); i++)
        image(i)= Color(0.5);
    
    // exemple : parcours tous les pixels de l'image
    for(int py= 0; py < image.height(); py++)
    for(int px= 0; px < image.width(); px++)
        // damier 8x8...
        if((px & 8) ^ (py & 8))
            image(px, py)= Color(1);
    
    // enregistre l'image, de plusieurs manieres...
    write_image_png(image, "image.png");
    write_image_bmp(image, "image.bmp");
    write_image_hdr(image, "image.hdr");
}

template <typename Func>
Image Draw(int w, int h, int samples, const Func& f)
{
    const int maxSamples = samples * samples;
    Image image(w, h);

    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            // Pixel de coordonnées (i, j).
            Point p = { (float)i, (float)j, 0.f };

            // Accumulateur dans le pixel
            Color accumulator(0.f, 0.f, 0.f, 0.f);

            // Samples dans le pixel
            for(int dx = 0; dx < samples; dx++)
            {
                for(int dy = 0; dy < samples; dy++)
                {
                    vec2 d = { 
                        (float)dx / (float)samples,
                        (float)dy / (float)samples
                    };

                    Point samplePos = { p.x + d.x, p.y + d.y, 0.f};
                    accumulator = accumulator + f(samplePos.x, samplePos.y);
                }
            }

            image(i, j) = Color(
                accumulator.r / maxSamples,
                accumulator.g / maxSamples,
                accumulator.b / maxSamples,
                accumulator.a / maxSamples
            );
        }
    }

    return image;
}

void Circle()
{
    constexpr int samples = 16;
    constexpr int maxSamples = samples * samples;
    Image image(1024, 512);

    const Point center = { image.width() / 2.f, image.height() / 2.f, 0.f };
    const float sqrR = std::pow(image.height() / 2.f, 2);
    const float sqrInnerR = std::pow(image.height() / 2.f - 5.145f, 2.f);
    const Color circleColor(1.f, 0.f, 0.f, 1.f);

    for(int i = 0; i < image.width(); i++)
    {
        for(int j = 0; j < image.height(); j++)
        {
            // Pixel de coordonnées (i, j).
            Point p = { (float)i, (float)j, 0.f };

            // Accumulateur dans le pixel
            size_t accumulator = 0;

            // Samples dans le pixel
            for(int dx = 0; dx < samples; dx++)
            {
                for(int dy = 0; dy < samples; dy++)
                {
                    vec2 d = { 
                        (float)dx / (float)samples,
                        (float)dy / (float)samples
                    };

                    Point samplePos = { p.x + d.x, p.y + d.y, 0.f};
                    if (distance2(samplePos, center) < sqrR && distance2(samplePos, center) > sqrInnerR)
                        accumulator++;
                }
            }

            float frac = (float)accumulator / (float)maxSamples;
            image(i, j) = Color(circleColor.r * frac, circleColor.g * frac, circleColor.b * frac, circleColor.a);
        }
    }

    write_image_png(image, "cercle.png");
}

void test_draw()
{
    constexpr int samples = 16;
    constexpr int w = 1024, h = 512;

    const Point center = { w / 2.f, h / 2.f, 0.f };
    const float sqrR = std::pow(h / 2.f, 2);
    const float sqrInnerR = std::pow(h / 2.f - 5.145f, 2.f);
    const Color circleColor(1.f, 0.f, 0.f, 1.f);

    auto f = [&](float x, float y)
    {
        if (distance2({x, y, 0.f}, center) < sqrR
            && distance2({x, y, 0.f}, center) > sqrInnerR)
            return circleColor;
        else
            return Color(0.f, 0.f, 0.f, 0.f);
    };

    write_image_png(Draw(w, h, samples, f), "test_cercle.png");
}

float Determinant(const vec2& u, const vec2& v)
{
    return u.x * v.y - u.y * v.x;
}

void DrawTriangle()
{
    constexpr int samples = 16;
    constexpr int w = 1024, h = 512;
    const Color triangleColor(1.f, 0.f, 0.f, 1.f);
    const Color bgColor(0.f, 0.f, 0.f, 1.f);

    const Vector A = { w / 3.f,       h / 3.f,       0.f };
    const Vector B = { 2.f * w / 3.f, h / 3.f,       0.f };
    const Vector C = { w / 2.f,       2.f * h / 3.f, 0.f };
    const vec2 AB = { B.x - A.x, B.y - A.y };
    const vec2 BC = { C.x - B.x, C.y - B.y };
    const vec2 CA = { A.x - C.x, A.y - C.y };

    auto f = [&](float x, float y) -> Color
    {
        const vec2 AP = { x - A.x, y - A.y };
        bool signe = Determinant(AB, AP) > 0;

        const vec2 BP = { x - B.x, y - B.y};
        if ((Determinant(BC, BP) > 0) != signe)
            return bgColor;
        
        const vec2 CP = { x - C.x, y - C.y };
        if ((Determinant(CA, CP) > 0) != signe)
            return bgColor;

        return triangleColor;
    };

    write_image_png(Draw(w, h, samples, f), "triangle.png");
}

int main( )
{
    DrawTriangle();
    
    return 0;
}
